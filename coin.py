def change(money):
    coin = [10, 5, 4, 2, 1]
    many_coin = [0, 0, 0, 0, 0]
    for i in range(len(coin)):
        if money >= coin[i]:
            many_coin[i] = money // coin[i]
            money = money % coin[i]
    return many_coin, coin

if __name__ == "__main__":
    money = int(input("Enter change: "))
    many_coin, coin = change(money)
    for i in range(len(coin)):
        print("เหรียญ {}: {} เหรียญ".format(coin[i], many_coin[i]))




