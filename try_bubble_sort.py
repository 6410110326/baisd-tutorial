def bubble_sort(data: list) -> list:
    sorted_data = data.copy()

    for i in range(len(sorted_data)):
        for j in range(len(sorted_data) - 1):
            if sorted_data[j] > sorted_data[j + 1]:
                sorted_data[j], sorted_data[j + 1] = sorted_data[j + 1], sorted_data[j]
    return sorted_data
        


if __name__ == "__main__":
    data = list(map(int, input('enter 10 number: ').split()))

    sorted_data = bubble_sort(data)

    print(sorted_data)
