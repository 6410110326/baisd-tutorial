import timeit

def recursive(n):
    if n < 2:
        return n
    else:
        return recursive(n-1) + recursive(n-2)

def bottom_up(n):
    f0 = 0 
    f1 = 1 
    f2 = 1
    for i in range(2, n+1):
        f2 = f0 + f1
        f0 = f1
        f1 = f2
    return f2

if __name__ == '__main__':
    n = int(input('Enter number of fibonacci number: '))
    print(
        "recursive fibonacci: ",
        recursive(n),
        timeit.timeit("recursive(n)", globals=globals()),
        )    
    print(
        "bottom-up fibonacci: ",
        bottom_up(n),
        timeit.timeit("bottom_up(n)", globals=globals()),
        )    
