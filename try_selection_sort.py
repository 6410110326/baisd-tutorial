def selection_sort(data: list) -> list:
    sorted_data = data.copy()
    
    for i in range(len(sorted_data) - 1, 0, -1):
        max_index = 0
        for j in range(1, i+1):
            if sorted_data[j] > sorted_data[max_index]:
                max_index = j
        sorted_data[i], sorted_data[max_index] = sorted_data[max_index], sorted_data[i]
    return sorted_data

if __name__ == '__main__':
    data = list(map(int, input('Enter number: ').split()))

    sorted_data = selection_sort(data)

    print(sorted_data)